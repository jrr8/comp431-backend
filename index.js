const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
var app = express();


// initialize database
const mongoClient = require('mongodb').MongoClient;
const mongoURI = 'mongodb://jrr8:test@ds259175.mlab.com:59175/heroku_k87m9szj';



function mc(execute) {
    var args = Array.prototype.slice.call(arguments, 1);

    mongoClient.connect(mongoURI, function(err, db) {
        if (err) {
            console.log('error in db action:', err, args)
        } else {
            args.unshift(db);
            execute.apply(null, args)
        }
    })
}

function makeOrQuery(field, values, useNumber=false) {

    var queryArray = [];

    function matchOne(id) {
        var newQuery = {};

        if (useNumber) {
            newQuery[field] = Number(id)
        } else {
            newQuery[field] = id
        }

        queryArray.push(newQuery);
    }

    values.forEach(matchOne);

    return queryArray

}

exports.mongoClient = mc;
exports.makeOrQuery = makeOrQuery;
exports.frontEndUrl = "https://obscene-tiger.surge.sh";
//exports.frontEndUrl = "http://localhost:4200";

app.use(bodyParser.json());
app.use(cookieParser());

const enableCORS = (req, res, next) => {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods','GET, PUT, POST, DELETE');
    res.header('Access-Control-Allow-Headers','Authorization, Content-Type, X-Request-With, X-Session-Id');
    res.header('Access-Control-Expose-Headers', 'Location, X-Session-Id');
    if(req.method === 'OPTIONS') {
        res.status(200).send("OK")
    } else {
        next()
    }
}

app.use(enableCORS);

const auth = require('./src/auth');
const articles = require('./src/articles');
const profile = require('./src/profile');
const following = require('./src/following');
//const img = require('./src/images');

auth(app);
articles(app);
profile(app);
following(app);
//img(app);

const port = process.env.PORT || 3000;
app.listen(port);
