// this is profile.js which contains all user profile
// information except passwords which is in auth.js

const makeOrQuery = require('../index').makeOrQuery;


function getHeadlines(db, req, res) {

    const users = req.params.users ? req.params.users.split(',') : [];

    var query = {};

    if (users.length > 0) {
        query = { $or: makeOrQuery('username', users, false) }
    }

    db.collection('profiles').find(query, function(err, r) {
        if (err) {
            return res.sendStatus(501)
        }

        r.toArray(function(err, documents) { return res.send(documents) })
    })

}

function updateHeadline(db, req, res) {

    db.collection('profiles').updateOne({ username: req.user.username }, { $set: { headline: req.body.headline }}, function(err, r) {
        if (err) {
            return res.sendStatus(501);
        }

        if (r.n === 0) {
            return res.send({usename: 'test', headline: 'headline'})
        }

        return res.send({ username: req.user.username, headline: req.body.headline });
    })

}

function getEmail(db, req, res) {

    const user = req.params.user ? req.params.user : req.user.username;

    db.collection('profiles').findOne({ username: user }, function(err, r) {
      if (err) {
          return res.sendStatus(501);
      }

      const email = r ? r.email : "";

      return res.send({ username: user, email: email })

    })
}

function updateEmail(db, req, res) {

    if (!req.body.email) { return res.send({ username: req.user.username, email: ""})}

    db.collection('profiles').updateOne({ username: req.user.username }, { $set: { email: req.body.email }}, function(err, _) {
        if (err) {
            return res.sendStatus(501);
        }

        res.send({ username: req.user.username, email: req.body.email })
    })

}

function getZip(db, req, res) {
    const user = req.params.user ? req.params.user : req.user.username;

    db.collection('profiles').findOne({ username: user }, function(err, r) {
        if (err) {
            return res.sendStatus(501);
        }

        const zip = r ? r.zipcode : "12345";

        return res.send({ username: user, zipcode: zip })

    })

}

function updateZip(db, req, res) {

    if (!req.body.zipcode) { return res.send({ username: req.user.username, zipcode: "12345"})}

    db.collection('profiles').updateOne({ username: req.user.username }, { $set: { zipcode: req.body.zipcode }}, function(err, _) {
        if (err) {
            return res.sendStatus(501);
        }

        res.send({ username: req.user.username, zipcode: req.body.zipcode })
    })
}

function getAvatars(db, req, res) {
    const users = req.params.users ? req.params.users.split(',') : [req.user.username];

    const query = makeOrQuery("username", users);
    var avatars = [];

    db.collection('profiles').find({ $or: query }).forEach(function(r) {
        avatars.push({ username: r.username, value: r.profileImg });
    }, function(err) {
        return res.send(avatars)
    })
}


function getDob(db, req, res) {
    db.collection('profiles').findOne({ username: req.user.username }, function(err, r) {
        if (err) {
            return res.sendStatus(501)
        }

        try {
            return res.send(new Date(r.dob))
        } catch(e) {
            return res.send("")
        }
    })
}


module.exports = function(app) {

    const mongo = require('../index.js').mongoClient;

    app.get('/headlines/:users?', function(req, res) { mongo(getHeadlines, req, res) } );
    app.put('/headline', function(req, res) { mongo(updateHeadline, req, res) } );
    app.get('/email/:user?', function(req, res) { mongo(getEmail, req, res)});
    app.put('/email', function(req, res) { mongo(updateEmail, req, res)});
    app.get('/zipcode/:user?', function(req, res) { mongo(getZip, req, res)});
    app.put('/zipcode', function(req, res) { mongo(updateZip, req, res)});
    app.get('/dob', function(req, res) { mongo(getDob, req, res) });
    app.get('/avatars/:users?', function(req, res) { mongo(getAvatars, req, res) });


};