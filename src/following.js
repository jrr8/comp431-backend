
function getFollowers(db, req, res) {
    res.send("stub for: \"" + req.path)
}

function updateFollowers(db, req, res) {
    res.send("stub for: \"" + req.path)
}

function deleteFollower(db, req, res) {
    res.send("stub for: \"" + req.path)
}


module.exports = function(app) {

    const mongo = require('../index.js').mongoClient;

    app.get('/following/:user?', function(req, res) { mongo(getFollowers, req, res) });
    app.put('/following/:user', function(req, res) { mongo(updateFollowers, req, res) });
    app.delete('/following/:user', function(req, res) { mongo(deleteFollower, req, res) });

};