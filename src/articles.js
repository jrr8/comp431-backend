const makeOrQuery = require('../index').makeOrQuery;


function getArticles(db, req, res) {
    if (req.params.id) {
        db.collection('articles').findOne({ '_id': req.params.id }, function(err, r) {
            if (err) {
                return res.sendStatus(501)
            }
            return res.send(r)
        })
    } else {
        db.collection('profiles').findOne({ username: req.user.username }, function(e, r) {
            if (!r) {
                return res.send([])
            }
            const authors = r.following ? r.following : [];
            authors.push(req.user.username);
            var query = { $or: makeOrQuery('author', authors) };
            var articles = [];
            db.collection('articles').find(query).limit(10).forEach(function(doc) {
                articles.push(doc)
            }, function(err) {
                return res.send(articles)
            })
        });
    }

}



function addArticle(db, req, res) {

    var article = {
        _id: null,
        author: req.user ? req.user.username : "default author",
        text: req.body.text,
        date: new Date().toJSON(),
        comments: [],
        img: null
    };

    db.collection('articles').count(function(_, r) {
        article._id = r + 1;

        db.collection('articles').insertOne(article, function(err, _) {
            if (err) {
                return res.sendStatus(400)
            }

            return res.send(article)
        })

    });
}

function putArticle(db, req, res) {
    db.collection('articles').findOne({ _id: Number(req.params.id) }, function(err, r) {
        if (err) {
            return res.sendStatus(501);
        }

        return res.send(r)
    })
}


module.exports = function(app) {

    const mongo = require('../index.js').mongoClient;

    app.get('/articles/:id?', function(req, res) { mongo(getArticles, req, res) });
    app.post('/article', function(req, res) { mongo(addArticle, req, res) });
    app.put('/articles/:id', function(req, res) { mongo(putArticle, req, res) });

};