const md5 = require('md5');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const frontEnd = require('../index').frontEndUrl;


// in-memory storage of cookie <-> user map
const sessionUser = {};
const cookieKey = 'sid';


const secretKey = md5('$3cr3t_pwd-BBy-0^');


function login(db, req, res) {

    const username = req.body.username;
    const password = req.body.password;

    if (!username || !password) {
        return res.sendStatus(400);
    }

    db.collection('users').findOne({ username: username }, function(err, r) {
        if (err) {
            return res.sendStatus(501);
        } else {
            if (!r || md5(password + r.salt) !== r.hash) {
                return res.sendStatus(401);
            }

            setSessionKey(res, r)
        }
    });
}


function setSessionKey(res, user, send=true) {
    const sessionKey = md5(secretKey + new Date().getTime() + user.username);
    user.sessionKey = sessionKey;
    sessionUser[sessionKey] = user;
    res.cookie(cookieKey, sessionKey, { maxAge:3600*1000, httpOnly: true });
    if (send) {
        res.send({ username: user.username, result: "success"});
    }
}


function logout(req, res) {
    if (!req.user) {
        return res.sendStatus(200);
    }
    delete sessionUser[req.user.sessionKey];
    res.clearCookie(cookieKey, { maxAge:3600*1000, httpOnly: true });
    return res.send("OK");
}


function register(db, req, res) {
    // extract request data
    const username = req.body.username? req.body.username: "";
    const password = req.body.password? req.body.password: "";
    const email = req.body.email? req.body.email: "";
    const dob = req.body.dob? req.body.dob: "";
    const zip = req.body.zip? req.body.zip: "";


    // add user auth info
    const salt = md5(secretKey + new Date().getTime());
    db.collection('users').updateOne( { username: username },  { $set: { username: username, salt: salt, hash: md5(password + salt) }},  { upsert: true },  function(err, _) {
        if (err) {
            return res.sendStatus(500);
        }
        // add user profile info
        const newInfo = {
            status: "dummy status",
            following: [],
            profileImg: "https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png",
            username: username,
            email: email,
            dob: dob,
            zip: zip
        };
        db.collection('profiles').updateOne({username: username}, { $set: newInfo }, { upsert: true }, function (err, _) {
            if (err) {
                return res.sendStatus(500);
            }
            res.send({result: "success", username: username})
        });
    });
}


function isLogggedIn(req, res, next) {
    if (['/auth/google','/login', '/register', '/logout'].includes(req.path)) {
        return next();
    }

    var sessionKey = req.cookies[cookieKey];
    var user = sessionUser[sessionKey];

    if (!user) {
        return res.sendStatus(401);
    }

    req.user = user;

    next()
}




module.exports = function(app) {

    const mongo = require('../index.js').mongoClient;

    passport.use(new GoogleStrategy({
            clientID        : "185895302525-97gu2bc40trk13enr4tl5pgshvuh5djh.apps.googleusercontent.com",
            clientSecret    : "MIIM7SdH8tSRAvPxVj16gtDA",
            callbackURL     : "https://chilling-asylum-94672.herokuapp.com/auth/google/callback"
        },
        function(accessToken, refreshToken, profile, done) {
            function inner(db) {
                db.collection('users').findOne({ email: profile.emails[0].value, auth: 'google' }, function(err, user) {
                    if (user) {
                        return done(err, user)
                    }

                    var newUser = { email: profile.emails[0].value, auth: 'google' };
                    db.collection('users').insertOne(newUser, function(err, _) {
                        return done(err, newUser)
                    })
                })
            }
            mongo(inner);
        }
    ));

    app.use(passport.initialize());

    app.get('/auth/google', passport.authenticate('google', { session: false, scope: ['openid profile email'] }));
    app.get('/auth/google/callback', passport.authenticate('google', { session: false, failureRedirect: '/' }),
        function(req, res) {
            setSessionKey(res, req.user, false);
            res.redirect(frontEnd);
        });

    // this applies isLoggedIn middleware globally
    app.use(isLogggedIn);

    app.post('/login', function(req, res) { mongo(login, req, res) });
    app.post('/register', function(req, res) { mongo(register, req, res) });
    app.put('/logout', logout);

};